function spasi() {
    console.log('');
    console.log('----------');
    
}


console.log('TUGAS 7 ES6');
spasi();
console.log('No. 1');
console.log('');


const golden = () => {
    console.log("This is golden!!")
};

golden();

spasi();
console.log('No. 2');
console.log('');

const newFunction = (namaAwal, namaAkhir) => {
    console.log(namaAwal + " " + namaAkhir);
};

newFunction("William", "Imoh")

spasi();
console.log('No.3');
console.log('');

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName,lastName,destination,occupation);


spasi();
console.log('No.4');
console.log('');

const west = ["WIll", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// console.log(combined);

const combinedArray = [...west, ...east]
console.log(combinedArray);

spasi();
console.log('No.5');
console.log('');

const planet = "earth"
const view = "glass"
var before = `Lorem  ${view}  dolor sit amet, ` +  
    `consectetur adipiscing elit, ${planet}  do eiusmod tempor ` +
    `incididunt ut labore et dolore magna aliqua. Ut enim` +
    `ad minim veniam`

//const before1 = `${planet} ${view}`
 
// Driver Code
console.log(before);

//console.log(before1); 

