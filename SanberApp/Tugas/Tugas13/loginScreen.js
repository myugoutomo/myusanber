import React, {Component} from 'react';
import {
    StyleSheet, 
    View, 
    Text, 
    Image, 
    TextInput, 
    Button,
    TouchableHighlight
} from 'react-native';

export default class App extends Component{
    render(){
        return(
            <View style={styles.container}>
                <Image source={require('./images/logo.png')} style = {{ width: 375, height: 102, top: 63}}/> 
                <Text style={styles.TitleRegis}>Register</Text>             

                <View style={styles.KolomInput}>
                    <View style= {styles.InputBox}>
                        <TextInput placeholder= "Username"></TextInput>
                    </View>
                    <View style= {styles.InputBox}>
                        <TextInput placeholder= "Password"></TextInput>
                    </View>
                </View>

                <View style={styles.ButtonMasuk}>
                <Button
                    title = "Masuk"
                    color = '#3EC6FF'
                    
                    // borderRadius = '60'
                    // width = '10'
                    //onPress={() => Alert.alert('Simple Button pressed')}
                />

                <Text> atau </Text>
                </View>
                <View style={styles.ButtonDaftar}>
                    <Button
                    title = "Daftar"
                    color = '#003366'
                   
                    // borderRadius = '60'
                    // width = '10'
                    //onPress={() => Alert.alert('Simple Button pressed')}
                    /> 
                </View>

              
                    
            </View>
            

        );
    }
} 

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    TitleRegis: {
        // width: 88,
        // height: 40,
        top: 80,
        left: 143,
        fontSize: 24,
        color: '#003366'
    },

    // KolomInput: {
    //     margin:10
    // },

    InputBox: {
        width: 294,
        height: 48,
        marginTop: 10,
        marginBottom: 10,
        top: 100,
        left: 40,
        borderColor: '#003366',
        //alignItems: 'center',
        borderWidth: 1
    },

    ButtonMasuk: {
        top: 130,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 50,
        color: '#003366',
        //width: 150,
        alignItems: 'center'

    },

    ButtonDaftar: {
        top: 130,
        marginTop: 5,
        marginBottom:5,
        borderRadius: 50,
        color: '#003366',
        //width: 150,
        alignItems: 'center'

    },

    submit:{
        top: 150,
        marginRight:40,
        marginLeft:40,
        marginTop:10,
        paddingTop:20,
        paddingBottom:20,
        backgroundColor:'#003366',
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#fff',
        alignItems: 'center'

    }

});