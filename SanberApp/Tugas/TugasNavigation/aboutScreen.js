import React, { Component } from 'react';
import {StyleSheet, View, Text, FlatList, ScrollView, Divider} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';



export default class App extends Component{
    render(){
        return(
            <View style={styles.container}>
                
                <View style={styles.header}>
                    <Text style={styles.title}>Tentang Saya</Text>
                    <View style={styles.userfoto}>
                        <Icon name="person" size={150} color={'#CACACA'} />
                    </View>
                    <Text style={styles.NamaUser}>M Yugo Utomo</Text>
                    <Text style={styles.titleUser}>React Native Developer</Text>
                </View>

                {/* <ScrollView> */}
                    <View style={styles.Portofolio}>
                        <Text style={{textAlign: 'left', color:'#003366', paddingLeft: 3}}>Portofolio</Text>
                        <View style={{flex:1, flexDirection: 'row', alignItems: 'center', padding:60}}>
                            <Icon style={{padding: 30}} name="person" size={50} color={'#CACACA'}  />
                            <Icon style={{padding: 30}} name="person" size={50} color={'#CACACA'} />
                        </View>

                        {/* <Divide style={{backgroundColor:'#003366', borderWidth:1}}/> */}
                    </View>

                    <View style={styles.HubungiSaya}>
                    <Text style={{textAlign: 'left', color:'#003366', paddingLeft: 3}}>Hubungi Saya</Text>
                        
                    <View style={{flex:1, flexDirection: 'row', alignItems: 'center', padding:60}}>
                            <Icon style={{padding: 30}} name="person" size={50} color={'#CACACA'}  />
                            <Icon style={{padding: 30}} name="person" size={50} color={'#CACACA'} />
                        </View>
                    </View>

                {/* </ScrollView> */}
                
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },

    title:{
        fontSize: 36,
        fontFamily: 'Roboto',
        padding: 20,
        color: '#003366',
        textAlign: 'center'
    },

    NamaUser:{
        fontSize: 24,
        fontFamily: 'Roboto',
        padding: 10,
        color: '#003366',
        textAlign: 'center'
    },

    titleUser:{
        fontSize: 16,
        fontFamily: 'Roboto',
        padding: 2,
        color: '#3EC6FF',
        textAlign: 'center'
    },

    header: {
        marginTop: 50,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        height: 150
        ,
        alignItems: 'center'
    },
    userfoto: {
        height: 200,
        width: 200,
        //position: 'absolute',
        bottom: 10,
        left: 5,
        backgroundColor: '#EFEFEF',
        borderRadius: 100,
        borderWidth: 1,
        borderColor: '#EFEFEF',
        alignItems: 'center',
        justifyContent: 'center',
    },

    Portofolio: {
        position: "absolute",
        width: 345,
        height: 140,
        left: 8,
        right: 8,
        top: 430,
        padding: 5,
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        
    },

    HubungiSaya: {
        position: "absolute",
        width: 345,
        height: 140,
        left: 8,
        right: 8,
        top:580,
        padding: 3,
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        
    },
});
