console.log('TUGAS 3');
console.log('==========');
console.log('');

console.log('No. 1 Looping While');
console.log('');

var number = 2;
console.log('Looping 1');
console.log('----------');
while (number <= 20) 
{
    console.log('%i - I Love Coding', number);
    number++;
} 


console.log('Looping 2');
console.log('----------');

var n = 20;
while (n >= 2 ) {
    console.log('%i - I Love Coding', n);
    n--;
}
console.log('');
console.log('==========');
console.log('');


console.log('No. 2 Looping For');
console.log('');

for(var angka = 1; angka <= 20; angka++){
    
    if ((angka%2)===0) {
      console.log(angka + ' - Berkualitas');
    }
    else if ((angka %3) === 0 && (angka%2)==1) {
        console.log(angka + ' - I Love Coding');
    }
    else {
        console.log(angka + ' - Santai');
    }
}

console.log('');
console.log('==========');
console.log('');
console.log('No. 3 Persegi Panjang');
console.log('');

let p = 4;
let l = 8;


if (p>2) {

    for (let i = 0; i < p; i++) {
        let result = '';
        //console.log('#');
        for (let j = 0; j < l ; j++) {
            result += '#';
        } 
        console.log(result);   
    }
}

console.log('');
console.log('==========');
console.log('');
console.log('No. 4 Membuat Tanggga');
console.log('');

for (let i = 1; i <= 7; i++) {
    let result = '';
    for (let j = i; j>=1; j--){
        result += '#';
    }
    console.log(result);
}

console.log('');
console.log('==========');
console.log('');
console.log('No. 5 Papan Catur');
console.log('');


let i ;
let j ;

    for (i = 1; i <=8; i++) {
        let result = '';

        for (j = 1; j <=8 ; j++) {
            if((i % 2 == 0 && j % 2 == 0) || (i % 2 == 1 && j % 2 == 1)) {
                result += ' ';
            }
         else result += '#';
            
        }

    
        console.log(result);   
    }



