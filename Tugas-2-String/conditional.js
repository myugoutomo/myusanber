console.log("TUGAS CONDITIONAL");
console.log("======================");

var nama = 'John';
var namaSatu = 'Jane';
var namaDua = 'Jenita';
var namaTiga = 'Junaedi';
var peran = '';
var peranSatu = 'Penyihir';
var peranDua = 'Guard';
var peranTiga = 'Warewolf';

// Output untuk Input nama = '' dan peran = ''
if (nama == '' || peran == ''){
    console.log("Nama harus diisi!");    
} 
console.log('');

//Output untuk Input nama = 'John' dan peran = ''
if (nama == 'John' || peran == ''){
    console.log('Halo %s, Pilih peranmu untuk memulai game!', nama);    
} else {
    console.log("Nama dan peran harus diisi!"); 
}
console.log('');

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
if (namaSatu == 'Jane' || peranSatu == 'Penyihir'){
    console.log('Selamat datang di Dunia Werewolf', namaSatu ); 
    console.log('Halo %s %s, kamu dapat melihat siapa yang menjadi werewolf!', namaSatu, peranSatu);    
} else {
    console.log("Nama dan peran harus diisi!"); 
}
console.log('');

//Output untuk Input nama = 'Jenita' dan peran 'Guard'
if (namaDua == 'Jenita' || peranDua == 'Guard'){
    console.log('Selamat datang di Dunia Werewolf', namaDua); 
    console.log('Halo %s %s, kamu akan membantu melindungi temanmu dari serangan werewolf.', namaDua, peranDua);    
} else {
    console.log("Nama dan peran harus diisi!");   
}
console.log('');

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
if (namaTiga == 'Junaedi' || peranDua == 'Warewolf'){
    console.log('Selamat datang di Dunia Werewolf',  namaTiga); 
    console.log('Halo %s %s Kamu akan memakan mangsa setiap malam!', namaTiga , peranTiga);    
} else {
    console.log("Nama dan peran harus diisi!"); 
}


console.log("");
console.log("");



console.log("TUGAS SWICTH CASE");
console.log("======================");

var hari = 21;
var bulan = 1;
var tahun = 1945;
var text;


switch (bulan) {
    case 1: { console.log(hari + ' Januari ' + tahun); break;}
    case 2: { console.log(hari + ' Februari ' + tahun); break;}
    case 3: { console.log(hari + ' Maret ' + tahun); break;}
    case 4: { console.log(hari + ' April ' + tahun); break;}
    case 5: { console.log(hari + ' Mei ' + tahun); break;}
    case 6: { console.log(hari + ' Juni ' + tahun ); break;}
    case 7: { console.log(hari + ' Juli ' + tahun); break;}
    case 8: { console.log(hari + ' Agustus ' + tahun); break;}
    case 9: { console.log(hari + ' September ' + tahun); break;}
    case 10: { console.log(hari + ' Oktober ' + tahun); break;}
    case 11: { uconsole.log(hari + ' November ' + tahun); break;}
    case 12: { console.log(hari + ' Desember ' +tahun); break;}
    default: { console.log('Bulan');}
    
    
}




console.log('');
