function spasi(){
    console.log('');
    console.log('==========');
    console.log('');

}

class Animal{
    constructor(name){
        this.name = name
        this.kaki = 4
        this.cold_blooded = false
    }
}

class Ape extends Animal {
    constructor (nama){
        super(nama);
        this.nama = nama;
        this.kaki =2
    }
yell(){
    if(this.kaki = 2)
    console.log('Auooo');
}
}

class Frog extends Animal{
    constructor (nama){
        super(nama);
        this.nama = nama;
        this.kaki = 2;
    }
    jump(){
        if(this.kaki = 2)
        console.log('hop hop');
    }
}

class Clock {
    constructor({template}){
        this.template = template;
    }

    render (){
        var date = new Date();

        var hours = date.getHours();
        if(hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if(mins < 10) mins = '0' + mins;
        
        var secs = date.getSeconds();
        if(secs < 10) secs = '0' + secs;

        var output = this.template 
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        
            console.log(output);
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}

console.log('TUGAS 9')
spasi();
console.log('---- No 1 ----');
console.log('')
var sheep = new Animal("shaun");

console.log(sheep.name);
console.log(sheep.kaki);
console.log(sheep.cold_blooded);

console.log('');
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

spasi();
console.log('---- No 2 ----');
console.log('')

var clock = new Clock({template: 'h:m:s'});
clock.start();